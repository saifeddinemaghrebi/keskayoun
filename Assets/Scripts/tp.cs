using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tp : MonoBehaviour
{
    [SerializeField] Transform Tp;
    
    [SerializeField] float TpTime;
    private float timer=0;
    // Start is called before the first frame update
    private void OnTriggerStay(Collider other)
    { if (other.gameObject.CompareTag("Player"))
            if (timer >= TpTime) {
                
                    other.transform.position = new Vector3(Tp.transform.position.x, Tp.transform.position.y, Tp.transform.position.z);
              
                timer = 0;
            }

            else { timer += Time.deltaTime;
                                 //todo start animation teleport
                                 }

    
    }

    private void OnTriggerExit(Collider other)
    {

        timer = 0;
 //todo kill animation teleport   
    }
    // Update is called once per frame
    IEnumerator Teleport(GameObject Player)
    {
        yield return new WaitForSeconds(1);
        Player.transform.position = new Vector3(Tp.transform.position.x, Tp.transform.position.y, Tp.transform.position.z);
    }
}

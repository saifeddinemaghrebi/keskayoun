using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Design;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

//attached to player
public class SkillHolder : MonoBehaviour
{
    //3 skills for the player to equip
    [SerializeField] public Skill skill1, skill2, skill3;
    //configurable keys for each skill
    [SerializeField] public Button key1, key2, key3; //TODO Convert to phone Buttons
    [SerializeField] private bool key1check,key2check, key3check;
    [HideInInspector] public enum playerEffects
    {
        noEffect,
        shrunk,
        enlarged,
    }
    [HideInInspector] public playerEffects effect;

    //for tracking the cooldowns and durations of each skill
    float cooldown1, cooldown2, cooldown3;
    float activeTime1, activeTime2, activeTime3;
    private void Start()
    {
        effect = playerEffects.noEffect;
        cooldown1 = 0;
        cooldown2 = 0;
        cooldown3 = 0;
        activeTime1 = 0;
        activeTime2 = 0;
        activeTime3 = 0;
        key1check= false;
        key2check = false;
        key3check = false;
    }

    private void Update()
    {
        //Calling State Handlers for cooldown management

        key1.onClick.AddListener(() =>
        {
            key1check = true;
        });
        key2.onClick.AddListener(() =>
        {
            key2check = true;
        });
        key3.onClick.AddListener(() =>
        {
            key3check = true;
        });
       
        
        skillStateHandler(skill1,ref key1check, ref activeTime1, ref cooldown1);
        skillStateHandler(skill2,ref key2check, ref activeTime2, ref cooldown2);
        skillStateHandler(skill3,ref key3check, ref activeTime3, ref cooldown3);
    }

    //for checking skill state and managing cooldowns
    //cooldown starts after active duration is over
    private void skillStateHandler(Skill s,ref bool keycheck,ref float activeTime,ref float cooldown )
    {
        switch (checkState (s))
        {
            case Skill.SkillState.ready:
                //skill can be cast and set as active when it's ready
                //duration can be 0 for instant skills
                if (keycheck == true)
                {
                    s.Activate(gameObject);
                    s.SetSkillState(Skill.SkillState.active);
                    activeTime = s.duration;
                    keycheck = false;
                }
                    //Debug.Log(key);
                    
                
                break;

            case Skill.SkillState.active:

                //managing the active time of the skill
                if (activeTime > 0)
                {
                    activeTime -= Time.deltaTime;
                    keycheck = false;
                }

                //deactivating and setting the cooldown when skill is done
                else
                {
                    s.Deactivate(gameObject);
                    s.SetSkillState(Skill.SkillState.cooldown);
                    cooldown = s.cooldown;
                    keycheck=false;
                }
                break;

            case Skill.SkillState.cooldown:

                //managing the cooldown of the skill
                if (cooldown > 0)
                {
                    cooldown -= Time.deltaTime;
                    keycheck = false;
                }

                //setting skill ready when cooldown is done
                else
                {
                    s.SetSkillState(Skill.SkillState.ready);
                    
                }
                break;
        }
    }

    //get the state of the skill
    private Skill.SkillState checkState(Skill s)
    {
        return s.GetSkillState();
    }

    //get The equipped Skills
    public List<string> getSkills()
    {
        List<string> result = new List<string>();
        result.Add(skill1.getSkillName());
        result.Add(skill2.getSkillName());
        result.Add(skill3.getSkillName());
        return result;
    }

    //Set the skills for each skill slot
    //intended for when equipping skills in the menu
    public void SetSkill1(Skill s)
    {
        skill1 = s;
    }

    public void SetSkill2(Skill s)
    {
        skill2 = s;
    }

    public void SetSkill3(Skill s)
    {
        skill3 = s;
    }
}

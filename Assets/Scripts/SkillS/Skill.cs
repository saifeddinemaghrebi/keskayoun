using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Skill : ScriptableObject
{
    [SerializeField] private string skillName;
    [SerializeField] private string description;
    [SerializeField] private Sprite icon;
    [SerializeField] public float cooldown; //seconds
    [SerializeField] public float duration;

    //declaring the possible states of the skill
    //possibly can add another "hover" state for previewing the casting during button hold
    public enum SkillState
    {
        ready, active, cooldown,
    }
    [SerializeField] public SkillState state = SkillState.ready;

    //Using the skill
    public virtual void Activate(GameObject parent) { }

    //ending the skill
    public virtual void Deactivate(GameObject parent) { }

    //public virtual string getSkill() { return ""; }

    public SkillState GetSkillState()
    {
        return state;
    }
    public void SetSkillState(SkillState s)
    {
        state = s;
    }

    public string getSkillName()
    {
        return this.skillName;
    }
}

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;
using static UnityEditor.PlayerSettings;
using System;
using Random = UnityEngine.Random;

public class LevelManager : MonoBehaviour
{
    [SerializeField] private NavMeshAgent agent;
    [SerializeField] private GameObject[] Zone1;
    [SerializeField] private GameObject[] Zone2;
    [SerializeField] private GameObject[] Zone3;
    [SerializeField] private GameObject Player;
    [SerializeField] private int timer;
    [SerializeField] private int nbMobsZ1;
    [SerializeField] private int nbMobsZ2;
    [SerializeField] private int nbMobsZ3;
    [SerializeField] private Dictionary< Vector3,bool> PositionZ1;
    [SerializeField] private Dictionary<Vector3,bool> PositionZ2;
    [SerializeField] private Dictionary< Vector3,bool> PositionZ3;

    //Events
    public static event Action OnGameStart;

    // Start is called before the first frame update
    void Start()
    {
        //   agent = GetComponent<NavMeshAgent>();
        OnGameStart += GenerateMobs;

        PileManager.OnPileBuilt += PlayerWin;
        OnGameStart?.Invoke();

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnDestroy()
    {
        OnGameStart -= GenerateMobs;
    }
    public void GenerateMobs()
    {
        PositionZ1 = new Dictionary<Vector3, bool>();
        PositionZ2 = new Dictionary<Vector3, bool>();
        PositionZ3 = new Dictionary<Vector3, bool>();
        foreach (var zone in Zone1)
        {
            PositionZ1.Add(zone.transform.position, false);
        }
        foreach (var zone in Zone2)
        {
            PositionZ2.Add(zone.transform.position, false);
        }
        foreach (var zone in Zone3)
        {
            PositionZ3.Add(zone.transform.position, false);
        }

        for (int i = 0; i < nbMobsZ1; i++)
        {
            spawnMob(PositionZ1.Keys.ToArray(),PositionZ1, nbMobsZ1);
        }
        for (int i = 0; i < nbMobsZ2; i++)
        {
            spawnMob(PositionZ2.Keys.ToArray(), PositionZ2, nbMobsZ2);
        }
        for (int i = 0; i < nbMobsZ3; i++)
        {
            spawnMob(PositionZ3.Keys.ToArray(), PositionZ3, nbMobsZ3);
        }
    }
    public void spawnMob(Vector3[] PositionList, Dictionary<Vector3, bool> ZONE,int nb)
    {
    
        Vector3 pos = PositionList[Random.Range(0, nb)];
        if (ZONE.ContainsValue(false)&&ZONE.Count>=nb)
        {
            if (ZONE[pos] == false)
            {
                GameObject MOB = Resources.Load("Zombie") as GameObject;
                Instantiate(MOB, pos, Quaternion.identity);
                ZONE[pos] = true;
            }
            else spawnMob(PositionList,ZONE,nb);
        }
        else Debug.Log("Zoneplaine");
    }
    public void spawnRock(Vector3 position)
    {
        GameObject ROCK = Resources.Load("Rock") as GameObject;
        Instantiate(ROCK, position, Quaternion.identity);
    }

    private void PlayerWin()
    {
        Debug.Log("Player wins");
    }
}

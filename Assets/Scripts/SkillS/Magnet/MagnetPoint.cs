using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagnetPoint : MonoBehaviour
{
    public float forceFactor = 1000f;

    public List<GameObject> balls;

    // Start is called before the first frame update
    void start()
    {
        balls = new List<GameObject>();
    }
    void FixedUpdate()
    {
        foreach (GameObject ball in balls)
        {
            ball.GetComponent<Rigidbody>().AddForce((transform.position - ball.transform.position) * forceFactor * Time.fixedDeltaTime);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Ball"))
            balls.Add(other.gameObject);

    }
    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Ball"))
            balls.Remove(other.gameObject);

    }

}

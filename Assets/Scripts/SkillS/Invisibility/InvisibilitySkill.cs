using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "InvisibilitySkill", menuName = "Scr�ptableObjects/Skill/Invisibility", order = 3)]
public class InvisibilitySkill : Skill
{
    private Renderer rend;
    public override void Activate(GameObject parent)
    {
        rend = parent.GetComponent<Renderer>();
        rend.enabled = false;
        //turning player invisible instantly
        //can be improved to make him turn invisible slowly
    }

    public override void Deactivate(GameObject parent)
    {
        rend = parent.GetComponent<Renderer>();
        rend.enabled = true;
        //turning player visible also can be improved to make it gradual
    }
}

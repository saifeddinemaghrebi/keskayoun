using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PController : MonoBehaviour
{
    //variables
    [SerializeField] private float Runspeed;
    [SerializeField] public float WalkSpeed;
    [SerializeField] public float moveSpeed;
    [SerializeField] private float JumpHeight;
    private Vector3 moveDirection;
    private Vector3 velocity;
    [SerializeField] private bool isGrounded;
    [SerializeField] private float groundChekDistance;
    [SerializeField] private LayerMask groundMask;
    [SerializeField] private float gravity;
    //REFERENCES
    private CharacterController controller;
    private Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        controller = GetComponent<CharacterController>();

        Debug.Log(controller);
        animator = GetComponentInChildren<Animator>();

    }

    // Update is called once per frame
    void Update()
    {
        Move();
        
    }

   /* private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Slime"))
        {
            Debug.Log("Slime");
        }
    }*/
    private void Move()
    {
        Vector3 feetPos = new Vector3(transform.position.x, transform.position.y - controller.height / 2, transform.position.z);
        isGrounded = Physics.CheckSphere(feetPos, groundChekDistance, groundMask);
        if (isGrounded && velocity.y < 0)
        {
            velocity = new Vector3(0,0,0);
            velocity.y = -2f;
        }
        float moveZ = Input.GetAxis("Vertical");
        float moveX = Input.GetAxis("Horizontal");
        moveDirection = new Vector3(0, 0, moveZ);
        moveDirection = transform.TransformDirection(moveDirection);
        if (isGrounded)
        {

            
            if (moveDirection != Vector3.zero && !Input.GetKey(KeyCode.LeftShift))
            {
                Debug.Log("is Wallking");
                //Walk
                Walk(moveZ);
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    Debug.Log("is Jumpping");
                    Jumpforward();
                }

            }
            else if (moveDirection != Vector3.zero && Input.GetKey(KeyCode.LeftShift))
            {

                Debug.Log("is Running");
                //run
                Run(moveZ);

            }
            else if (moveDirection == Vector3.zero)
            {
                //Idle
                Idle();
                if (moveX != 0)
                {
                    if (moveX > 0)
                    {
                        animator.SetFloat("Blend", 5f);
                    }
                    else
                    {
                        animator.SetFloat("Blend", 6f);
                    }
                }
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    Debug.Log("is Jumpping");
                    Jump();
                }

            }
            moveDirection *= moveSpeed;

        }

        controller.Move(moveDirection * Time.deltaTime);
        velocity.y += gravity * Time.deltaTime;
        

        controller.Move(velocity * Time.deltaTime);

    }
    private void Idle()
    {
        //float x = animator.GetFloat("Blend");
        
        
            
               animator.SetFloat("Blend", 0f);
            /*Debug.Log(x);*/
    }
    private void Walk(float Movez)
    {

        moveSpeed = WalkSpeed;
        if (Movez > 0f)
        {
           
                animator.SetFloat("Blend", 1f);
        }
        if (Movez < 0f)
        {
            
                animator.SetFloat("Blend", 2f);
        }
    }
    private void Run(float Movez)
    {
        moveSpeed = Runspeed;
        if (Movez > 0f)
            animator.SetFloat("Blend", 3f);
        if (Movez < 0f)
           animator.SetFloat("Blend", 4f);
    }
    private void Jumpforward()
    {
        velocity = transform.forward * 10 * Input.GetAxis("Vertical");
        float jumpY = Mathf.Sqrt(JumpHeight * -2 * gravity);
        velocity.y = jumpY;
        animator.SetFloat("Blend", 7f);

    }
    private void Jump()
    {

        float jumpY = Mathf.Sqrt(JumpHeight * -2 * gravity);
        velocity.y = jumpY;
        animator.SetFloat("Blend", 7f);

    }
    public void superJump(float height)
    {

        float jumpY = Mathf.Sqrt(height * -2 * gravity);
        velocity.y = jumpY;
        //animator.SetFloat("Blend", 3f);

    }
    
}

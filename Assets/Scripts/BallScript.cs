using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallScript : MonoBehaviour
{
    [SerializeField] private bool picabale;
    [SerializeField] private bool inZone;
    [SerializeField] private GameObject Player;
    //[SerializeField] public GameObject Target;
    [SerializeField] private GameObject BallContainer;
    
    private Transform containaireTransform;
    public Transform parent;
    [SerializeField] public Rigidbody rb;
    [SerializeField] private  GameObject Ball;
     private Transform Balltrasform;
    private Vector3 moveDirection;
    [SerializeField] private GameObject levelManager;

    public event Action OnMobDeath;

    public bool Picabale { get => picabale; set => picabale = value; }

    void Awake()
    {
       // parent = Target.transform;
       
    }
     void Start()
    {
        
        inZone = false; 
        Picabale = true;
        Balltrasform = Ball.GetComponent<Transform>();
        rb = GetComponentInChildren<Rigidbody>();
        moveDirection = new Vector3(0, 0, 1);
    }
     void Update()
    {
        if(!Picabale)
        moveDirection = transform.TransformDirection(moveDirection);
        if(Ball.transform.position.y<=-10)
            Ball.transform.position = new Vector3(-274,0,180);

    }
    private void OnTriggerEnter(Collider other)
    {
       
        if(other.gameObject.tag=="Player")
        inZone = true;
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            Player = other.gameObject;
            if (inZone)
            {
                if (Picabale)
                {
                     Picabale = false;

                       PlayerController pcontroller = Player.GetComponent<PlayerController>();
                    GameObject Hand = pcontroller.Hand;
                        transform.position = Hand.transform.position;
                        transform.rotation = Hand.transform.rotation;
                        transform.SetParent(Hand.transform);
                        rb.useGravity = false;
                        rb.isKinematic = true;
                        // Balltrasform.position = new Vector3(0,0,0);            
                        // Debug.Log(Player);
                        pcontroller.setHasBall();


                    
                }
            }
            if (!Picabale)
            {
                //if (Input.GetKey(KeyCode.Q))
                //{
                //    Debug.Log("drop");

                //    transform.SetParent(null);
                //    Picabale = true;
                //    rb.isKinematic = false;
                //    rb.useGravity = true;

                //    transform.position = Balltrasform.position;

                //    // Debug.Log(Player);
                //    Player.GetComponent<PlayerController>().DropBall();
                //}
            }
        }
        
    }
    private void OnTriggerExit(Collider other)
    {
        if(other.gameObject.tag=="Player")
        inZone = false;
    }
    public void releaseme()
    {
       /****/
    }
    public void setpicable()
    {
        Picabale = true;
    }

    public void setInzone()
    {
        inZone = true;
    }
    private void OnCollisionEnter(Collision collision)
    {
        Picabale = true;
        if (collision.gameObject.tag == "Mob")
        {
            Debug.Log("mobdeath");
            LevelManager level = levelManager.GetComponent<LevelManager>();
            level.spawnRock(collision.gameObject.transform.position);
            Destroy(collision.gameObject);
            OnMobDeath?.Invoke();
        }
    }
}

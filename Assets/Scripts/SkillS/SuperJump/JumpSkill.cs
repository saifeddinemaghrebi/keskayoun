using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "JumpSkill", menuName = "Scr�ptableObjects/Skill/SuperJump", order = 2)]
public class JumpSkill : Skill
{
    [SerializeField] float jumpForce; //instant jump force
    public override void Activate(GameObject parent)
    {
        parent.GetComponent<PlayerController>().superJump(jumpForce);
    }
}

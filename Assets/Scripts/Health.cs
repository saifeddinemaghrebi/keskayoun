using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Health : MonoBehaviour
{
    private float health;
    private float lerpTimer;
    public float chipspeed = 2f;
    public float maxHealth = 100f;
    public Image frontHealthBar;
    public Image backHealthBar;
    public Animator animator;
    public float conteur;
    [SerializeField] private GameObject spawnPoint;
    // Start is called before the first frame update
    void Start()
    {
        health = maxHealth;
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        health = Mathf.Clamp(health, 0, maxHealth);
        UpdateHealthUI();    
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "BigTrap")
        {
            TakeDamage(25);
        }
        if (other.gameObject.tag == "SmallTrap")
        {
            TakeDamage(10);
        }
        if (other.gameObject.tag == "Health")
        {
            RestoreHealth(10);
        }
      

    }
    public void  UpdateHealthUI()
    {
        Debug.Log(health);
        float fillF = frontHealthBar.fillAmount;
        float fillB = backHealthBar.fillAmount;
        float hFraction = health / maxHealth;
        if(fillB> hFraction)
        {
            frontHealthBar.fillAmount = hFraction;
            frontHealthBar.color = Color.red;
            lerpTimer += Time.deltaTime;
            float percentComplete = lerpTimer / chipspeed;
            percentComplete = percentComplete * percentComplete;
            backHealthBar.fillAmount = Mathf.Lerp(fillB, hFraction, percentComplete);
        }
        if(fillF<hFraction)
        {
            backHealthBar.color = Color.green;
            backHealthBar.fillAmount = hFraction;
            lerpTimer += Time.deltaTime;
            float percentComplete = lerpTimer / chipspeed;
            percentComplete = percentComplete * percentComplete;
            frontHealthBar.fillAmount = Mathf.Lerp(fillF, backHealthBar.fillAmount, percentComplete);
        }
       


    }
    public void TakeDamage(float damage)
    {
        health -= damage;
        lerpTimer = 0f;
        if (health <= 0)
        {
            Debug.Log("Dead");
            Death();
        }
      //  Destroy(this.gameObject);
    }
    public void RestoreHealth(float healAmount)
    {
        health += healAmount;
        lerpTimer = 0f;

    }
   public void Death()
    {
       
        
            animator.SetTrigger("dead");
        
    }    
    public void WaitAndRevive()
    {
        wait(conteur);
        gameObject.SetActive(false);
        wait(1);
        animator.SetTrigger("revive");
        transform.position = spawnPoint.transform.position;
        health = maxHealth;
        gameObject.SetActive(true);
    }
    IEnumerator wait(float DelayTime) {
        yield return new WaitForSeconds(DelayTime);
    }
}

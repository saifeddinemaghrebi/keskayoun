using System.Collections;
using System.Collections.Generic;
using UnityEditor.PackageManager;
using UnityEngine;
using System;

public class PileManager : MonoBehaviour
{
    [SerializeField] 
    private GameObject[] prefabs; //prefab for each pile state [0 .. 7]

    private int maximumRocks, currentRocks;

    [HideInInspector] 
    public static event Action OnPileBreaking; //Event to declare the breaking of the Pile

    [HideInInspector]
    public static event Action OnPileBuilt; //Event to declare the successful building of the Pile

    [HideInInspector]
    public static event Action OnAddedRock; //Event to declare adding rock but not completing build
    private void Start()
    {
        maximumRocks = prefabs.Length-1;
        LevelManager.OnGameStart += setMobMode;
        KesKayounMatchEvents.OnGameStart += setKesKayounMode;

        switchRocks(currentRocks);
    }

    // when ball collides while at max rocks, break pile
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Ball"))
        {
            breakPile();

        }
    }

    private void setMobMode()
    {
        currentRocks = 0;
    }
    private void setKesKayounMode()
    {
        currentRocks = maximumRocks;
    }
    //set the pile to rock count and set the according prefab active
    private void switchRocks(int rockCount)
    {
        if(rockCount <= maximumRocks)
        {
            //deactivate all prefabs first
            foreach (GameObject prefab in prefabs)
            {
                prefab.SetActive(false);
            }
            //then activate the corresponding prefab
            prefabs[rockCount].SetActive(true);

            if (rockCount == maximumRocks) OnPileBuilt?.Invoke(); //Announce The Pile Completely Built
            else OnAddedRock?.Invoke(); //Announcing Added Rock
        }
    }

    //removes all rocks from pile
    public void breakPile()
    {
        if(currentRocks == maximumRocks)
        {
            currentRocks = 0;
            switchRocks(currentRocks); //Update prefab
            OnPileBreaking?.Invoke(); //Announce the PileBreaking
        }
    }

    //adds a rock to the Pile
    public bool addRockToPile()
    {
        if (currentRocks < maximumRocks)
        {
            currentRocks++;
            switchRocks(currentRocks); //Update prefab
            return true;
        }
        return false;
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MobMvt : MonoBehaviour
{
    //variable 
    [SerializeField] private NavMeshAgent agent;
    [SerializeField] GameObject player;
    [SerializeField] private GameObject[] targets;
    [SerializeField] private bool HasDistination;
    [SerializeField] private bool memoriseplayerPosition;
    [SerializeField] private float playerMemoryTimer;
    private float timer;
    [SerializeField] private float remembermeTime;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        agent = GetComponentInParent<NavMeshAgent>();
        memoriseplayerPosition=false;
        HasDistination = false;
        timer = 0;

    }

    // Update is called once per frame
    void Update()
    {
        if (!HasDistination)
        {
            GetDistination();
        }
        else if (HasDistination)
        {
            if (timer < 30) { timer+=Time.deltaTime; }
            else { HasDistination=false; timer = 0; }
        }
        if (memoriseplayerPosition)
        {
            targetThePlayer();
            playerMemoryTimer += Time.deltaTime;
            if(playerMemoryTimer > remembermeTime)
            {
                forgetthePlayer();
            }
        }

    }
    public void GetDistination()
    {
        agent.SetDestination(targets[Random.Range(0, targets.Length)].transform.position);
        HasDistination = true;
    }
    public void targetThePlayer()
    {
        agent.SetDestination(player.transform.position);

    }
    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            HasDistination=true;
 
            targetThePlayer();
        }
    }
    public void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            HasDistination = true;
          
            targetThePlayer();
        }
    }
    public void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            memoriseplayerPosition = true;

            
        }

    }
    public void forgetthePlayer()
    {
        memoriseplayerPosition=false;
        HasDistination = false;
        playerMemoryTimer = 0;
    }

}

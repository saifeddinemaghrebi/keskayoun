using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "DashSkill", menuName = "Scr�ptableObjects/Skill/Dash", order = 2)]
public class DashSkill : Skill
{
    [SerializeField] private float dashDistance;

    public override void Activate(GameObject parent)
    {
        Debug.Log("here");
        parent.GetComponent<CharacterController>().Move(parent.transform.forward * dashDistance);
    }
}

using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting.Antlr3.Runtime.Tree;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    // Start is called before the first frame update

    //variables
    [SerializeField] private float Runspeed;
    [SerializeField] public float WalkSpeed;
    [SerializeField] public float moveSpeed;
    [SerializeField] private float JumpHeight;
    private Vector3 moveDirection;
    private Vector3 velocity;
    [SerializeField] private bool isGrounded;
    [SerializeField] private float groundChekDistance;
    [SerializeField] private LayerMask groundMask;
    [SerializeField] private float gravity;
    [SerializeField] private bool HasBall;
    public FixedJoystick Leftjoystick;
    public FixedJoystick RightJoystick;
    public Button btn;
    public Button throwBall;
    public Toggle run;
    public GameObject Hand;
    private bool isRaning;
    //REFERENCES
    private CharacterController controller;
    private Animator animator;
    private GameObject Ball;

    // Start is called before the first frame update
    void Start()
    {
        Ball = GameObject.FindGameObjectWithTag("Ball");
        controller = GetComponent<CharacterController>();

      //  Debug.Log(controller);
        animator = GetComponentInChildren<Animator>();
        isRaning = false;

    }

    // Update is called once per frame
    void Update()
    {
        if (run.isOn)
        {
            isRaning = true;
        }
        else
        {
            isRaning=false;
        }
        if (HasBall)
        {
            throwBall.onClick.AddListener(mythrow);

            //if (Input.GetKey(KeyCode.Z))
            //{
            //    animator.SetTrigger("throw");
            //}
        }
        
           
            Move();
        

    }

    /* private void OnCollisionEnter(Collision collision)
     {
         if (collision.gameObject.CompareTag("Slime"))
         {
             Debug.Log("Slime");
         }
     }*/
    private void Move()
    {
        Vector3 feetPos = new Vector3(transform.position.x, transform.position.y - controller.height / 2, transform.position.z);
        isGrounded = Physics.CheckSphere(feetPos, groundChekDistance, groundMask);
        if (isGrounded && velocity.y < 0)
        {
            velocity = new Vector3(0, 0, 0);
            velocity.y = -2f;
        }
        float moveZ = Leftjoystick.Vertical;
        float moveX = Input.GetAxis("Horizontal");
        moveDirection = new Vector3(0, 0, moveZ);
        moveDirection = transform.TransformDirection(moveDirection);
        if (isGrounded)
        {


            //   if (moveDirection != Vector3.zero && !Input.GetKey(KeyCode.LeftShift))
            if (moveDirection != Vector3.zero && !isRaning)
            {
               // Debug.Log("is Wallking");
                //Walk
                Walk(moveZ);
                btn.onClick.AddListener(Jumpforward);
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    //  Debug.Log("is Jumpping");
                    // Jumpforward();
                  

                }

            }
            // else if (moveDirection != Vector3.zero && Input.GetKey(KeyCode.LeftShift))
            else if (moveDirection != Vector3.zero && isRaning)
            {

               // Debug.Log("is Running");
                //run
                Run(moveZ);

            }
            else if (moveDirection == Vector3.zero)
            {
                //Idle
                Idle();
                if (moveX != 0)
                {
                    if (moveX > 0)
                    {
                        animator.SetFloat("Blend", 6f);
                    }
                    else
                    {
                        animator.SetFloat("Blend", 5f);
                    }
                }
                /*if (Input.GetKeyDown(KeyCode.Space))
                {
                    Debug.Log("is Jumpping");
                    Jump();
                }*/
                btn.onClick.AddListener(Jump);


            }
            moveDirection *= moveSpeed;

        }

        controller.Move(moveDirection * Time.deltaTime);
        velocity.y += gravity * Time.deltaTime;


        controller.Move(velocity * Time.deltaTime);

    }
    private void Idle()
    {
        //float x = animator.GetFloat("Blend");

        

        animator.SetFloat("Blend", 0f);
        /*Debug.Log(x);*/
    }
    private void Walk(float Movez)
    {

        moveSpeed = WalkSpeed;
        if (Movez > 0f)
        {

            animator.SetFloat("Blend", 3f);
        }
        if (Movez < 0f)
        {

            animator.SetFloat("Blend", 4f);
        }
    }
    private void Run(float Movez)
    {
        moveSpeed = Runspeed;
        if (Movez > 0f)
            animator.SetFloat("Blend", 1f);
        if (Movez < 0f)
            animator.SetFloat("Blend", 2f);
    }
    private void Jumpforward()
    {
        // velocity = transform.forward * 10 * Input.GetAxis("Vertical");
        velocity = transform.forward * 10 * Leftjoystick.Vertical;
        
        float jumpY = Mathf.Sqrt(JumpHeight * -2 * gravity);
        velocity.y = jumpY;
        animator.SetFloat("Blend", 7f);

    }
    private void Jump()
    {

        float jumpY = Mathf.Sqrt(JumpHeight * -2 * gravity);
        velocity.y = jumpY;
        animator.SetFloat("Blend", 7f);

    }
    public void superJump(float height)
    {

        float jumpY = Mathf.Sqrt(height * -2 * gravity);
        velocity.y = jumpY;
        //animator.SetFloat("Blend", 3f);

    }
    public void setHasBall()
    {
        HasBall = true;
        animator.SetTrigger("pickBall");
    }
    public void DropBall()
    {
        HasBall = false;
        animator.SetTrigger("dropBall");
    }
    public void ThrowBall()
    {
        BallScript bscript = Ball.GetComponent<BallScript>();
        bscript.setpicable();
        bscript.rb.useGravity = true;
        bscript.rb.isKinematic = false;
       Ball.transform.SetParent(null);
        if (moveDirection == Vector3.zero)
        {
            moveDirection = Vector3.forward;
            moveDirection = transform.TransformDirection(moveDirection);
        }
            bscript.rb.AddForce(moveDirection * 80000 * Time.deltaTime);
        bscript.Picabale = false;

    }
    public void mythrow()
    {
       
      animator.SetTrigger("throw");
           
    }



}
